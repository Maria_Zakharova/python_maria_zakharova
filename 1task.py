def func(imput_value):
    """Функция осуществляет проверку делимости введенного числа на 3 и 5.

    Принимает на вход положительное целое число.

    Возвращает:
    "foo" - если это число делится на 3;
    "bar" - если это число делится на 5;
    "foo bar" - если это число делится на 3 и на 5;
    строку с этим числом во всех остальных случаях.

    """
    assert imput_value % 1 == 0, 'Ошибка! Введите целое число!'
    assert imput_value >= 0, 'Ошибка! Введите положительное число!'

    if imput_value % 3 == 0 and imput_value % 5 == 0:
        return 'foo bar'
    elif imput_value % 3 == 0:
        return 'foo'
    elif imput_value % 5 == 0:
        return 'bar'
    else:
        return str(imput_value)


if __name__ == '__main__':
    assert func(15) == 'foo bar'
    assert func(25) == 'bar'
    assert func(24) == 'foo'
    assert func(17) == '17'
